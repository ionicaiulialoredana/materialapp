﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MatApp.Models;

namespace MatApp.Data
{
    public class MatAppContext : DbContext
    {
        public MatAppContext (DbContextOptions<MatAppContext> options)
            : base(options)
        {
        }

        public DbSet<MatApp.Models.Material> Material { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Material>()
                .HasIndex(p => new { p.ErpCode })
                .IsUnique(true);
        }
    }
}
