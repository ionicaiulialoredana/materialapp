﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MatApp.Models
{
    public class Material
    {
        public int ID { get; set; }

        [Required]
        public string ErpCode { get; set; }

        public string Description { get; set; }


        [Range(1, 10000000000)]

        public int Stock { get; set; }

    }
}

